## About

This is a technical test for the Front End / Wordpress Developer position.

## Installation

**Requirements:**

An installation of WordPress is required.

Steps:

1. Run *git clone https://williamfmc@bitbucket.org/williamfmc/elaniin-foodies.git*

2. Copy the folder Foodies to your WordPress themes folder *wp-content/themes*

  
3. Create the following pages in Wordpress:

	1. Landing (*/landing*)
	2. Acerca De (*/acerca-de*)
	3. Restaurantes (*/restaurantes*)
	4. Menú (*/menu*)
	5. Contáctanos (*/contactanos*)  

4. Set the Homepage under Settings > Reading and choose Landing as the homepage.

5. Create a new menu under Appearance > Menus and assign it to the Foodie location. Add the following pages:

	1. Acerca de
	2. Restaurantes
	3. Menú
	4. Contáctanos

6. Activate the Foodies theme under Appearance > Themes

## Technologies

For the development of this theme I used the following:

1. WordPress
2. PHP
3. jQuery
4. CSS (SASS)
5. Bootstrap 4

All necessary scripts and libraries are already enqueued and there is no need to reference external files.

