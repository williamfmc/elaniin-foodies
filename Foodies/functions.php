<?php
	

// Define the version so we can easily replace it throughout the theme
define( 'WM_VERSION', 1.0 );

/*-----------------------------------------------------------------------------------*/
/*  Set the maximum allowed width for any content in the theme
/*-----------------------------------------------------------------------------------*/
if ( ! isset( $content_width ) ) $content_width = 1500;

/*-----------------------------------------------------------------------------------*/
/* Register main menu for Wordpress use
/*-----------------------------------------------------------------------------------*/
register_nav_menus( 
	array(
		'primary'	=>	__( 'Foodie Menu', 'main-menu' ), // The Primary menu
		
	)
);

/*-----------------------------------------------------------------------------------*/
/* Enqueue Styles and Scripts
/*-----------------------------------------------------------------------------------*/

function foodie_scripts()  { 

	// WP style.css
	//wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/style.css');
	
	// Bootstrap 4
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/libs/bootstrap/css/bootstrap.min.css');

	
	// add theme scripts
	wp_enqueue_script( 'foodies-js', get_template_directory_uri() . '/js/theme.min.js', array('jquery'), WM_VERSION, true );
	//Bootstrap 4 bundle
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/libs/bootstrap/js/bootstrap.bundle.min.js');

	
  
}
add_action( 'wp_enqueue_scripts', 'foodie_scripts' ); 

//Since Foodies is a fair small site, having separate stylesheets for each page will help in performance (it will not load unused css from other pages)

//Register stylesheets
function register_pages_css(){
	wp_register_style("landing", get_template_directory_uri() . "/sass/landing.css");
	wp_register_style("acerca-de", get_template_directory_uri() . "/sass/acercaDe.css");
	wp_register_style("restaurantes", get_template_directory_uri() . "/sass/restaurantes.css");
	wp_register_style("menu", get_template_directory_uri() . "/sass/menu.css");
	wp_register_style("contactanos", get_template_directory_uri() . "/sass/contactanos.css");
}
add_action('wp_loaded', 'register_pages_css');
// just enqueue current page

function enqueue_page_foodies(){
	$currentPage = basename(get_permalink());
	
	switch ($currentPage) {
		case 'acerca-de':
			wp_enqueue_style('acerca-de');
			break;

		case 'restaurantes':
			wp_enqueue_style('restaurantes');
		break;

		case 'menu':
			wp_enqueue_style('menu');
		break;

		case 'contactanos':
			wp_enqueue_style('contactanos');
		break;

		default:
		wp_enqueue_style('landing');
		break;
	}

}
add_action('wp_enqueue_scripts', 'enqueue_page_foodies');