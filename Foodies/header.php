<?php
	//Foodie header
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <title>
        <?php bloginfo('name'); // show the blog name, from settings ?> |
        <?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings  ?>
    </title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


    <?php // add support for HTML5 elements in older IE versions. ?>
    <!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

    <?php wp_head(); 

?>

</head>

<body <?php body_class(); //Body Starts ?>>

    <header id="masthead" class="site-header">
        <div class="container center">

            <nav class="site-navigation main-navigation">
                <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); // Display the user-defined menu in Appearance > Menus ?>
            </nav><!-- .site-navigation .main-navigation -->
        </div>
        <div class="center">

            <div id="brand">
                <h1 class="site-title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); // Link to the home page ?>"
                        title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">Foodies</a>
                </h1>
                <h4 class="site-description">
                    <?php bloginfo( 'description' ); // Display the blog description, found in General Settings ?>
                </h4>
            </div><!-- /brand -->

            <div class="clear"></div>
        </div>
        <!--/container -->

    </header><!-- #masthead .site-header -->

    <main class="main-fluid">
        <!-- start the page containter -->