<?php
	//Foodie header
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <title>
        <?php bloginfo('name'); // show the blog name, from settings ?> |
        <?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings  ?>
    </title>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


    <?php // add support for HTML5 elements in older IE versions. ?>
    <!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

    <?php wp_head(); 

?>

</head>

<body <?php body_class(); //Body Starts ?>>
    <div class="container page-container">
        <header id="foodies-header" class="site-header light-header">
            <!-- <div class="container-fluid clearfix "> -->
            <div class="container-fluid head-container clearfix">
                <div class="top-bar">
                    <div class="foodies-logo">
                        <a class="logo"
                            href="<?php echo esc_url( home_url( '/' ) ); // Link to the home page ?>">Foodies</a>
                    </div>
                    <nav class="site-navigation main-navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); // Display the user-defined menu in Appearance > Menus ?>
                    </nav><!-- .site-navigation .main-navigation -->
                    <div class="mobile-menu-btn">
                        <img src="<?php echo get_template_directory_uri() . "/images/mobile-menu-btn.png"; ?>" alt=""
                            class="mobile-btn">
                    </div>
                    <?php ///////// Mobile menu. this is hidden in desktop views /////////// ?>
                    <div class="mobile-nav-container">
                        <div class="clearfix logo-exit">
                            <div class="mob-nav-col-l">
                                <h2>
                                    <a class="mobile-menu-logo"
                                        href="<?php echo esc_url( home_url( '/' ) ); // Link to the home page ?>">Foodies</a>
                                </h2>
                            </div>
                            <div class="mob-nav-col-r">
                                <div class="close-mob-menu">
                                    <img src="<?php echo get_template_directory_uri() . "/images/close.png"; ?>" alt=""
                                        class="close-btn">
                                </div>
                            </div>
                        </div>
                        <div class="mobile-main-menu">
                            <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                        </div>
                    </div>

                </div>
                <div class="row hero-content">
                    <div class="col-md-6 hero-copy-column order-sm-2 order-md-1 order-last">
                        <h1 class="hero-title">
                            Un nuevo sabor est&aacute; en la ciudad
                        </h1>
                        <p class="hero-paragraph">
                            Estamos a punto de descubrir un mundo lleno de sabores y de emociones inigualables.
                        </p>
                        <div class="hero-btn">
                            <a href="./restaurantes/" class="fill-btn">Encuentranos <img
                                    src="<?php echo get_template_directory_uri() . "/images/arrow-right.png"; ?>" alt=""
                                    class="arrow-right"></a>
                        </div>
                    </div>
                    <div class="col-md-6 hero-image-column order-sm-1 order-md-2 order-first">
                        <img src="<?php echo get_template_directory_uri() . "/images/hero-hamburger.png"; ?>"
                            alt="Big McFoodie" class="hero-image">
                    </div>
                </div>
            </div>


            <!-- </div> -->
        </header><!-- #masthead .site-header -->

        <main class="main-fluid">
            <!-- start the page containter -->