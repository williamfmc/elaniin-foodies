<section class="about-section">
    <div class="about-container clearfix">
        <div class="about-left">
            <div class="left-container vh-100">
                <h2 class="comida-arte">
                    LA COMIDA ES<br><span class="nuestro-arte">NUESTRO ARTE</span>
                </h2>
            </div>
        </div>

        <div class="about-right">
            <div class="right-container">
                <h3 class="about-title">
                    ¿Quién es Foodies?
                </h3>
                <p class="about-paragraph">
                    Elit irure ad nulla id elit laborum nostrud mollit irure. Velit reprehenderit sunt nulla enim
                    aliquip duis tempor est culpa fugiat consequat culpa consectetur Lorem. Reprehenderit dolore culpa
                    irure eiusmod minim occaecat et id minim ullamco.
                </p>
                <div class="about-btn">
                    <a href="./contactanos/" class="fill-btn">Encuentranos <img
                            src="<?php echo get_template_directory_uri() . "/images/arrow-right.png"; ?>" alt=""
                            class="arrow-right"></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div class="map-section-container clearfix">
        <div class="map-controls-container">
            <div class="map-controls vh-100 clearfix">
                <h2 class="controls-title">
                    Estamos para ti
                </h2>
                <div class="controls-type-selection clearfix">
                    <button class="control-btn btn-llevar" name="llevar" type="button" value="takeaway"><span
                            class="btn-icon"><?php echo file_get_contents(get_template_directory_uri()."/images/food.svg"); ?></span>Para
                        Llevar</button>
                    <button class="control-btn btn-domicilio" name="domicilio" type="button" value="delivery"><span
                            class="btn-icon"><?php echo file_get_contents(get_template_directory_uri()."/images/truck.svg"); ?></span>Domicilio</button>
                </div>
                <div class="map-search">
                    <img src="<?php echo get_template_directory_uri() . "/images/search.png"; ?>" alt=""
                        class="search-icon"><input type="text" class="map-input"
                        placeholder="Buscar nombre o direcci&oacute;n">
                </div>
                <div class="location-list">

                </div>
            </div>
        </div>
    </div>
</section>