<?php
	/*-----------------------------------------------------------------------------------*/
	/* This template will be called by all other template files to finish 
	/* rendering the page and display the footer area/content
	/*-----------------------------------------------------------------------------------*/
?>

</main><!-- / end page container, it was open in the header -->

<footer class="site-footer">
    <div class="site-info container">

        Created by William Manzano

    </div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->

<?php wp_footer(); 
// Used for plugins
?>
</div><?php // Ends page container. It was open in the header ?>
</body>

</html>